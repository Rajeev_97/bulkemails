public with sharing class BulkEmails {
     public static void sendMail(String Subject,List < Contact > listOfContacts,String Body) {
        Id orgWideDefaultId=EmailManager.getFromAddress();
        Messaging.Email[] messages = new Messaging.Email[0];
        for(Contact con:listOfContacts){
        if(con.Email != null){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {con.Email};
            mail.setToAddresses(toAddresses);
            If(orgWideDefaultId != null){
                mail.setOrgWideEmailAddressId(orgWideDefaultId);
            }
            mail.setSubject(Subject);
            mail.setHtmlBody('Hello '+ con.FirstName+' '+con.LastName+',<br/><br/>'+Body);
            messages.add(mail);
         }
        }
        Messaging.sendEmail(messages, false);
    }
    public static Id getFromAddress(){
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'DefaultAddress'];
        if ( owea.size() > 0 ) {
           return owea.get(0).Id;
        }
        return null;
    }

}